module Main (main) where

import Lib ( app )

import Test.Hspec ( hspec, describe, it, Spec )
import Test.Hspec.Wai ( get, shouldRespondWith, with )

main :: IO ()
main = hspec spec

spec :: Spec
spec = with (return app) $ do
    describe "GET /" $ do
        it "responds with 200" $ do
            get "/" `shouldRespondWith` 200
        it "responds with a User" $ do
            let user = "{\"age\":25,\"name\":\"Ivan\"}"
            get "/user/Ivan/25" `shouldRespondWith` user
