module Lib where

import Servant ((:<|>)((:<|>)), Server, serve)
import HelloApi (User(User), helloApi, HelloAPI)
import Servant.Server (Application)

server :: Server HelloAPI
server = hello :<|> user
    where
        hello = return "Hello world"
        user n a = return (User n a)

app :: Application
app = serve helloApi server
